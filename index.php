<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Angular JS Insert Update Delete Using PHP MySQL</title>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            .pointer span{
                cursor: pointer;
            }
        </style>
    </head>
    <body  cz-shortcut-listen="true" ng-app="userManage" ng-controller="BaseController" ng-init="init()">
        <input type="hidden" id="base_path" value="http://localhost/angular-php/"> 
        <div class="container">
            <h2 class="title text-center"> AngularJS PHP MySQL</h2>
            <div class="col-md-4 col-xs-12 ">
                <form role="form" method="POST">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" placeholder="Name" id="name" class=" form-control">
                    </div>
                    <div class="form-group">
                      <label for="email">Email address:</label>
                      <input type="email" placeholder="Email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                      <label for="company-name">Company Name:</label>
                      <input type="text" placeholder="Company Name" class="form-control" id="company-name">
                    </div>
                    <div class="form-group">
                      <label for="company-name">Designation:</label>
                      <input type="text" placeholder="Designation" class="form-control" id="designation">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="col-xs-12 col-md-8 table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <td>
                                #ID
                            </td>
                            <td>
                                Name
                            </td>
                            <td>
                                Email
                            </td>
                            <td>
                                Company name
                            </td>
                            <td>
                                Designation
                            </td>
                            <td>
                                Action
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="user in data.users">
                            <td>{{user.id}}</td>
                            <td>{{user.name}}</td>
                            <td>{{user.email}}</td>
                            <td>{{user.companyName}}</td>
                            <td>{{user.designation}}</td>
                            <td><button ng-click="edit(user.id)">Edit</button> |<button ng-click="delete(user.id)">Delete</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script type="text/javascript" src="js/angular-route.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
