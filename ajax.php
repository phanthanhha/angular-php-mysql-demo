<?php
require_once 'config.php';
if(isset($_POST['type'])){
    $type = $_POST['type'];
    switch ($type){
        case 'getUsers':
            getUsers($mysqli);
            break;
        case 'saveUsers':
            saveUsers($mysqli);
        default :
        break;
    }
    
    
}

function getUsers($mysqli){
    try{
        $query = 'SELECT *FROM '.DATABASE.' ORDER BY id ASC';
        $result= $mysqli->query($query);
        $data = array();
        while ($row = $result->fetch_assoc()){
            $data['data'][] = $row;
        }
        $data['success']=true;
        echo json_encode($data);
        exit();
        
    } catch (Exception $e) {
        $data= array();
        $data['success'] = FALSE;
        $data['message'] = $e->getMessage();
        echo json_encode($data);
        exit();
    }
}

