var app = angular.module('userManage', []);

var base_path = $('#base_path').val();
app.controller('BaseController', function ($scope, $http) {
    var url = base_path + 'ajax.php';
    $scope.data = {};
    $scope.data.users = [];

    $scope.init = function () {
        $http({
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param({type: 'getUsers'})
        }).success(
            function(response){
                if(response.success && !angular.isUndefined(response.data)){
                    $scope.data.users = response.data;
                }else{
                    console.log('fail to load data');
                }
            }
        );
    }
    
    
    

});

