var test = {
    firstName:'hehe',
    lastName:'hihi',
    run:function(){
        console.log(this.firstName+this.lastName);
    }
};
test.run();
var app = angular.module('testApp',['ngRoute']);
app.config(function($routeProvider){
    
    $routeProvider
    .when('/',{
        templateUrl:'partial/test1.html',
        controller:'listController'
    })
    .when('/:name',
    {
        templateUrl:'partial/test2.html',
        controller:'detailController'
    })
    .otherwise(
    {
        redirectTo:'/'
    }
    );
});
app.factory('services',function($http){
    return {
        list: function(callback){
            $http.get('countries.json').success(callback);
        },
        find: function(id,callback){
            $http({
                method: "GET",
                url: "countries.json",
                cache: true
            }).success(callback);
        }
    }
});
app.controller('listController',function($scope,$http,services){
    services.list(function(countries){
        $scope.countries=countries;
    });
});
app.controller('detailController',function($scope, $routeParams,$http){
    $scope.detail = $routeParams.name;
    $http.get('countries.json').success(function(data){
        $scope.country = data.filter(function(entry){
            return entry.name === $scope.detail;
        })[0];
        
    });
});