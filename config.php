<?php
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD','');
define('DATABASE', 'employee');

$mysqli= new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

if(mysqli_connect_errno()){
    echo "fail to connect: ".mysqli_connect_errno();
    exit();
}
